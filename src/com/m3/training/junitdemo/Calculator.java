package com.m3.training.junitdemo;

public class Calculator {
	static final int CHAR_LIMIT = 1024;
	public Calculator() {

	}

	public Double evaluate(String input) {
		if (input == null || input.trim().isEmpty()) {
			String msg = "Null or empty input is not permitted.";
			throw new IllegalArgumentException(msg);
		}
		if (input.length() > Calculator.CHAR_LIMIT) {
			String msg = "The given input exceeded the character limit of " + Calculator.CHAR_LIMIT;
			throw new IllegalArgumentException(msg);
		}
		return Double.parseDouble(input);
	}

}
